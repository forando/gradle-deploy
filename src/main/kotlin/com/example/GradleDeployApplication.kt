package com.example

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class GradleDeployApplication

fun main(args: Array<String>) {
    SpringApplication.run(GradleDeployApplication::class.java, *args)
}
